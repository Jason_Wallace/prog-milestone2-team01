﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_4
{
    class Program
    {
        static void Main(string[] args)
        {

            rate();
            ask("n");
            checks(dict);
        }


        //THIS METHOD BASICALLY INTRODUCE PROGRAM TO USER
        static void rate()
        {
            Console.Clear();
            Console.WriteLine("--RATE YOU FAVORITE APP--");
            Console.WriteLine();
            Console.WriteLine("Enter in 5 food following by rating 5 to 1");
        }


        
        
        //THIS IS THE METHOD WHERE PROGRAM ASK USER QUESTIONS
        static string val;
        static string key;
        static Dictionary<string, string> dict = new Dictionary<string, string>();


        static void ask(string status)
        {


            while (status == "n")
            {
                Console.WriteLine("Enter the Item Name");
                key = Console.ReadLine();
                Console.WriteLine("Enter the Rating (1-5)");
                val = Console.ReadLine();
                dict.Add(key, val);

                Console.WriteLine("Press 'n' to continue, 'enter' to exit");
                status = Console.ReadLine();

                if (status == "n")
                {
                    checks(dict);
                }
                

                option();
                dict.Clear();
                change("y");
            }
        }

        
        
        
        // checks if user entered 5 food 
        static void checks(Dictionary<string, string> dict)
        {
            if (dict.Count == 5)
            {
                Console.WriteLine("You have entered 5 foods already, press enter to see your favorite food and their ratings.");
                ask("enter");
            }
            else
            {
                Console.WriteLine("keep going");
                ask("n");
            }



            Console.WriteLine();
            Console.WriteLine($"The total number of items in the dictionary = {dict.Count}");
        }

        //most favorite food to least
        static void option()
        {

            Console.WriteLine("Items Sort by Most Favorite Food");
            foreach (var item in dict.OrderByDescending(k => k.Value))
                Console.WriteLine("{0}-{1}", item.Key, item.Value);
        }



        //Asking user again if they want to change any item in the food.
        static void change(string status)
        {

            while (status == "y")
            {
                Console.WriteLine("Hey would you like to change prefrence of your favorite food!");
                Console.WriteLine("Press 'y' then");
                status = Console.ReadLine();

                if (status == "y")
                {
                    ask("n");
                }
            }

        }
    }
}



























